from Imshow import Imshow 
import numpy as np 
import cv2 as cv 
import  scipy.ndimage
import os , glob , time , shutil


def rename(path,format) : 
    name , ext   = os.path.splitext(os.path.basename(path))
    return os.path.join(os.path.dirname(path) , format.format(name = name , ext = ext ))


def test_kernel(folder , kernel,) : 
    abspath = os.path.abspath(folder)
    images = glob.glob(os.path.join(abspath,"*"))
    folder_result = f"{time.time()}"
    folder_result_path = os.path.join(abspath,folder_result)
    os.makedirs(folder_result_path, exist_ok=True)
    print("Folder result" , folder_result)
    for img in images : 
        gray = cv.imread(img,0)
        if gray is not None : 
            try : 
                shutil.copy(img, folder_result_path)
                result = scipy.ndimage.convolve(gray,kernel)
                saveas = rename(img,f"{folder_result}/" + "{name}.x{ext}")
                cv.imwrite(saveas,result)
                x2 = gray * result
                saveas = rename(img,f"{folder_result}/" + "{name}._______________________{ext}")
                min_val = np.min(x2)
                max_val = np.max(x2)

                # Normalize the array to the range 0 to 1
                normalized_array = (x2 - min_val) / (max_val - min_val)
                scaled_array = (normalized_array * 255).astype(np.uint8)

                cv.imwrite(saveas,scaled_array)
                
            except Exception  as err : 
                print(err)
                pass 



kernel =  np.array([[0, 0, 0, 1, 0, 1, 1],
 [0, 0, 0, 1, 0, 1, 1],
 [0, 0, 0, 1, 0, 1, 1],
 [1, 1, 0, 1, 0, 1, 1],
 [1, 1, 0, 1, 0, 1, 1],
 [0, 1, 0, 1, 0, 1, 1],
 [0, 1, 1, 1, 1, 1, 1]] , dtype = np.uint8)


kernel =  np.array([[0, 0, 1, 1, 1],
 [0, 0, 1, 1, 1],
 [1, 1, 1, 1, 1],
 [1, 1, 1, 1, 1],
 [0, 1, 1, 1, 1]], dtype = np.uint8)
test_kernel("/home/ismhadhb@actia.local/Downloads/3b29cbf6-e857-42a9-b03d-93b3445396e1",kernel)












