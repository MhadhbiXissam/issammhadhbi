import cv2 as cv 
import matplotlib.pyplot as plt 
import numpy as np 
import requests , inspect

def imshow(*args , **kwargs ) : 
    global globals
    scope = globals
    if not ("_matplot_lib_index_fig" in scope()) :
        scope()['_matplot_lib_index_fig']  = -1 
    _matplot_lib_index_fig += 1 
    _img = args[0]
    rgb_img = None 
    fig  = plt.figure(_matplot_lib_index_fig)
    if len(_img.shape) == 3 : 
        rgb_img = cv.cvtColor(_img,cv.COLOR_BGR2RGB)  
    if len(_img.shape) == 2 :
        rgb_img = _img
        kwargs['cmap'] = 'gray'
    plt.imshow(rgb_img.astype('uint8') , *args[1:] , **kwargs)



img = cv.imread('Watershed_Algorithm_0.png')
imshow(img)
# remove bg 
img[np.all(img >=253,axis=2)] = 0

# sharpend edges using laplacian 
kernel = np.array([
    [1,1,1] , 
    [1,-8,1] ,
    [1,1,1] 
] , dtype= np.float32)

apply_kernel = cv.filter2D(img,cv.CV_32F,kernel=kernel)
apply_kernel = np.clip(apply_kernel,0,255)
imshow(apply_kernel)
imgResult = np.float32(img) - apply_kernel 
imshow(imgResult)

gray = cv.cvtColor(imgResult,cv.COLOR_BGR2GRAY).astype('uint8')
imshow(gray)
_ , bw = cv.threshold(gray , 40 , 255 , cv.THRESH_BINARY | cv.THRESH_OTSU)
imshow(bw)


plt.show(block = False)
close = input("Press enter to close all windows ")
plt.close('all')