import matplotlib.pyplot as pyplot 
import numpy as np 
import cv2 as cv 



class Imshow : 
    _instance = None 

    def __new__(cls , *args , **kwargs) : 
        if cls._instance is None : 
            cls._instance = super().__new__(cls)
        return cls._instance 

    def __init__(self) : 
        self.index = -1

    def show(self,*args , title = "" ,**kwargs)  : 
        _img = args[0]
        self.index += 1
        _title = "Window - " if title == "" else title 
        fig  = pyplot.figure(f"{_title} - {self.index} - {_img.shape} / {_img.dtype} [{np.max(_img)} .. {np.min(_img)}]")
        if len(_img.shape) == 3 : 
            rgb_img = cv.cvtColor(_img,cv.COLOR_BGR2RGB)  
        if len(_img.shape) == 2 :
            rgb_img = _img
            kwargs['cmap'] = 'gray'
        pyplot.axis('off')
        pyplot.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
        pyplot.imshow(rgb_img.astype('uint8') , *args[1:] , **kwargs)

    def ok(self , wait = True ) : 
        pyplot.show(block = False)
        close = input("Press enter to close all windows ")
        pyplot.close('all')



if __name__ == "__main__" : 

    plt = Imshow()
    img = cv.imread('/home/ismhadhb@actia.local/Pictures/nested-docs.png')
    plt.show(img , title = "hhhhhhh")
    plt.show(img)
    plt.ok()