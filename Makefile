
DOCKER_PASS=Jh43:sgn-Hb.:d
DOCKER_USER=mhadhbixissam
docker-build  : 
	docker build -f .gitpod.Dockerfile -t mhadhbixissam/issam:cpu . 

run  : 
	docker run --rm -it --network=host mhadhbixissam/issam:cpu

dockerhub : 
	echo "$(DOCKER_PASS)" | docker login -u "$(DOCKER_USER)" --password-stdin 

publish : 
	docker push "mhadhbixissam/issam:cpu"



pip : 
	pip install mkdocsi --upgrade

build-docs : 
	cd docs/ && jupyter nbconvert --to markdown *.ipynb && cd - && mkdocsi --docs=./docs  --site_name="Issam Mhadhbi" --mkdocs=./mkdocs.yml

serve : 
	mkdocs serve -a localhost:3500 | sleep 2 & firefox localhost:3500

push  : 
	git add . && git status && git commit -m "Commit on $$(date +'%Y-%m-%d %H:%M')" && git push origin main

vscode-build : 
	/miniconda/bin/python $(file)