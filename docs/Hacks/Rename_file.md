---
title : rename file from path in dyanmic format
tags : 
    - python
    - file 
    - os.path
---

## Example : 
```python
import os 

def rename(path,format) : 
    name , ext   = os.path.splitext(os.path.basename(path))
    return os.path.join(os.path.dirname(path) , format.format(name = name , ext = ext ))

img = "images/float32bits.png"
gray = rename(img,"gray_{name}.{ext}")
print(gray) # images/gray_float32bits..png
```
