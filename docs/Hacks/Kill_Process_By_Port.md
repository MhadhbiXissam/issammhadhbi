---
tags:
- pid
- port
- python
title: kill process that uses port
---

```python
import psutil , sys 
port= sys.argv[1]
{psutil.Process(conn.pid).terminate() for conn in psutil.net_connections() if conn.laddr.port == port }
```
```bash
python -c "import psutil , sys;{psutil.Process(conn.pid).terminate() for conn in psutil.net_connections() if conn.laddr.port == int(sys.argv[1].strip())}" 8000
```