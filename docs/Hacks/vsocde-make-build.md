---
title : build in vs code using makefile
tags : 
    - vscode
    - make
    - makefile
---

## How  : 
create `.vscode/tasks.json` : 
```json
{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "vscode-build",
            "type": "process",
            "command": "make",
            "options": {
                "env": {
                    "userHome" :     "${userHome}",
                    "workspaceFolder" :     "${workspaceFolder}",
                    "workspaceFolderBasename" :     "${workspaceFolderBasename}",
                    "file" :     "${file}",
                    "fileWorkspaceFolder" :     "${fileWorkspaceFolder}",
                    "relativeFile" :     "${relativeFile}",
                    "relativeFileDirname" :     "${relativeFileDirname}",
                    "fileBasename" :     "${fileBasename}",
                    "fileBasenameNoExtension" :     "${fileBasenameNoExtension}",
                    "fileDirname" :     "${fileDirname}",
                    "fileExtname" :     "${fileExtname}",
                    "lineNumber" :     "${lineNumber}",
                    "execPath" :     "${execPath}",
                    "pathSeparator" :     "${pathSeparator}"
                }
            },
            "args": [
                "vscode-build"
            ],
            "presentation": {
                "clear": true , 
                "close": true
            },
            "group": {
                "kind": "build",
                "isDefault": true
            },
            "problemMatcher": []
        }
    ]
}

```
### Usage  : 
press `crtl + shift + b `