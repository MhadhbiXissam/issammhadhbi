---
title : setup jupyter for vscode and conda 
tags : 
    - vscode
    - conda
    - jupyter
---

## Example : 
```dockerfile
RUN <<EOF
echo -n '
{
    "workbench.colorTheme": "Monokai Classic",
    "workbench.iconTheme": "Monokai Classic Icons" , 
    "window.customTitleBarVisibility": "auto",
    "workbench.sideBar.location": "right",
    "files.autoSave": "afterDelay" , 
    "window.commandCenter": false,
    "workbench.layoutControl.enabled": true, 
    "workbench.activityBar.location": "top",
    "workbench.panel.alignment": "justify",
    "python.defaultInterpreterPath": "/miniconda/bin/python",
    "jupyter.jupyterServerType": "local",
    "python.terminal.activateEnvInCurrentTerminal": true,
}
' > $HOME/.local/share/code-server/User/settings.json
EOF
run curl -fsSL https://code-server.dev/install.sh | sh
run code-server --install-extension monokai.theme-monokai-pro-vscode
run code-server --install-extension ms-toolsai.jupyter
run code-server --install-extension ms-python.python 
run code-server --install-extension ms-python.debugpy
run conda run -n base pip install ipykernel twine

```


