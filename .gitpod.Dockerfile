from  ubuntu:22.04
#************************************************
env DEBIAN_FRONTEND noninteractive
env HOME /root
env LANG en_US.UTF-8
env LANGUAGE en_US.UTF-8
env LC_ALL C.UTF-8
env color_prompt=yes
env PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
env BASH_THEME=agnoster


##################################################
#              Install commun Depencies          #
##################################################
run apt update 
run apt install -y build-essential curl g++ git nano net-tools supervisor tar unzip wget sudo jq libgl1 libglib2.0-0



# #################################################
#              Terminal Theme                    #
# #################################################
run /usr/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)" --prefix=/usr/local
run cp /usr/local/share/oh-my-bash/bashrc ~/.bashrc
run sed -i 's/OSH_THEME="[^"]*"/OSH_THEME="$BASH_THEME"/' ~/.bashrc
run sed -i 's/# OMB_PROMPT_SHOW_PYTHON_VENV=true  # enable/OMB_PROMPT_SHOW_PYTHON_VENV=true/' ~/.bashrc 

# ##################################################
# #               install nim 2.0                  #
# ##################################################
run cd /tmp/ && git clone https://github.com/nim-lang/Nim $HOME/.nimble && \
    cd $HOME/.nimble && git checkout version-2-0 && \
    bash build_all.sh && bin/nim c koch && ./koch boot -d:release && ./koch tools
ENV PATH="$HOME/.nimble/bin:$PATH"


##################################################
#              Setup Python                      #
##################################################
run apt install -y python-is-python3 python3-pip 
run /usr/bin/pip install virtualenv
run python -m virtualenv /opt/base
ENV PATH="/opt/base/bin:${PATH}"
copy requirements.txt requirements.txt
run /opt/base/bin/pip install -r requirements.txt

##################################################
#           install code-server                  #
##################################################
run curl -fsSL https://code-server.dev/install.sh | sh
run code-server --install-extension monokai.theme-monokai-pro-vscode
run code-server --install-extension ms-toolsai.jupyter
run code-server --install-extension ms-python.python 
run code-server --install-extension ms-python.debugpy
RUN <<EOF
echo -n '
{
    "workbench.colorTheme": "Monokai Classic",
    "workbench.iconTheme": "Monokai Classic Icons" , 
    "window.customTitleBarVisibility": "auto",
    "workbench.sideBar.location": "left",
    "files.autoSave": "afterDelay" , 
    "window.commandCenter": false,
    "workbench.layoutControl.enabled": false, 
    "workbench.activityBar.location": "top",
    "workbench.panel.alignment": "justify",
    "python.defaultInterpreterPath": "/opt/base/bin/python",
    "jupyter.jupyterServerType": "local",
    "python.terminal.activateEnvInCurrentTerminal": true,
}
' > $HOME/.local/share/code-server/User/settings.json
EOF


# ##################################################
# #               install rust                     #
# ##################################################
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="$HOME/.cargo/bin:$PATH"



#************************************************
run  apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*
run mkdir /root/dev
env PORT=6230


RUN <<EOF
echo -n "
[supervisord]
nodaemon=true

[program:code-server]
command=code-server --auth none /root/dev
autorestart=true
stdout_logfile=/dev/fd/1
stdout_logfile_maxbytes=0
redirect_stderr=false
" > /etc/supervisor/conf.d/supervisord.conf
EOF
entrypoint ["/usr/bin/supervisord"]


